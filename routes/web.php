<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

//AULA: Começando com rotas amigáveis

Route::get('/minha-rota', function () {
    return 'Olá Mundo!';
});

//Rota com parametro opcional {name?}, é preciso definir o valor padrão, nesse caso, null.
Route::get('/rota-parametro/{id}/{name?}', function ($id, $name = null) {
    return "Cliente: $id, $name";
});

//AULA: Rotas amigáveis Posts e Formulários

Route::get('client', function () {
    $csrfToken = csrf_token();
    $action = route('client.store');
    $html = <<<HTML
    <html>
        <body>
            <form method="post" action="$action">
                <input type="hidden" name="_token" value="$csrfToken">
                Nome: <input type="text" name="value">
                <button type="submit">Enviar</button>
            </form>
        </body>
    </html>
HTML;

    return $html;
});

Route::post('client', function (Request $request) {

    return $request->value;
})->name('client.store');


//AULA: Iniciando com blade

Route::get('/client-form', function () {
    return view('client.client-form');
})->name('client-form');

Route::post('/client-form', function(Request $request) {
    $nome = trim(strip_tags($request->name));
    return view('client.client-dados', compact('nome', 'request'));
    })->name('client-data');

